import 'package:flutter/material.dart';
import 'package:multipath/screens/feature_screen2.dart';

class FeatureOne extends StatefulWidget {
  const FeatureOne({Key? key}) : super(key: key);

  @override
  State<FeatureOne> createState() => _FeatureOneState();
}

class _FeatureOneState extends State<FeatureOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature One"),
        centerTitle: true,
      ),
      body: Stack(
        children: [buildDashboardRoute(context)],
      ),
    );
  }

  Positioned buildDashboardRoute(BuildContext context) {
    return Positioned(
        child: Column(
      children: [
        ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const FeatureTow()));
            },
            child: const Text("one")),
      ],
    ));
  }
}


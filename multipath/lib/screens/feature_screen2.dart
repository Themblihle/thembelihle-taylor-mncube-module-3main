import 'package:flutter/material.dart';
import 'package:multipath/screens/feature_screen1.dart';

class FeatureTow extends StatefulWidget {
  const FeatureTow({Key? key}) : super(key: key);

  @override
  State<FeatureTow> createState() => _FeatureOneState();
}

class _FeatureOneState extends State<FeatureTow> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature Two"),
        centerTitle: true,
      ),
      body: Stack(
        children: [buildDashboardRoute(context)],
      ),
    );
  }

  Positioned buildDashboardRoute(BuildContext context) {
    return Positioned(
        child: Column(
      children: [
        ElevatedButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const FeatureOne()));
            },
            child: const Text("Two")),
      ],
    ));
  }
}


import 'package:flutter/material.dart';
import 'package:multipath/models/palette.dart';
import 'package:multipath/screens/feature_screen1.dart';

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  bool isMale = true;
  bool isLoginScreen = true;
  bool isRememberMe = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: const Color.fromARGB(255, 104, 158, 203),
        title: const Text("Profile"),
        elevation: 4, centerTitle: true,
      ),
      body: Stack(children: [
        Positioned(
            //bottom: isLoginScreen ? 200 : 230,
            top: isLoginScreen ? 200 : 230,
            child: Container(
                height: isLoginScreen ? 465 : 450,
                padding: const EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width - 40,
                margin: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        blurRadius: 15,
                        spreadRadius: 4,
                      )
                    ]),
                child: SingleChildScrollView(
                  child: Column(children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isLoginScreen = true;
                            });
                          },
                          child: Column(
                            children: [
                              Text(
                                "Profile Edit",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: isLoginScreen
                                        ? Palette.activeColor
                                        : Palette.textColor1),
                              ),
                              if (isLoginScreen)
                                Container(
                                  margin: const EdgeInsets.only(top: 3),
                                  height: 2,
                                  width: 55,
                                  color: const Color.fromARGB(255, 0, 187, 255),
                                )
                            ],
                          ),
                        )
                      ],
                    ),
                    if (isLoginScreen) buildSignUp(),
                  ]),
                ))),
        buildBottomContainer(true),
        buildBottomContainer(false),
      ]),
    );
  }

  Container buildSignUp() {
    return Container(
      margin: const EdgeInsets.only(top: 25),
      child: Column(
        children: [
          buildTextField(Icons.person_outline, "First Name", false, false),
          buildTextField(Icons.people_outline, "Last Name", false, false),
          buildTextField(Icons.person_outline, "Age", false, false),
          buildTextField(
              Icons.phone_android_outlined, "Phone Number", false, false),
          buildTextField(Icons.mail_outline, "info@gmail.com", false, true),
          buildTextField(Icons.lock_outline, "Password", true, false),
        ],
      ),
    );
  }

//buttons
  Positioned buildBottomContainer(bool shadows) {
    return Positioned(
      top: isLoginScreen ? 620 : 480,
      right: 0,
      left: 0,
      child: Center(
          child: Column(
        children: [
          const Padding(padding: EdgeInsets.all(10)),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const FeatureOne()),
              );
            },
            child: const Text("SUBMIT"),
          )
        ],
      )),
    );
  }

  Widget buildTextField(
      IconData icon, String hintText, bool isPassword, bool isEmail) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: TextField(
          obscureText: isPassword,
          keyboardType:
              isEmail ? TextInputType.emailAddress : TextInputType.text,
          decoration: InputDecoration(
            hintText: hintText,
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Palette.textColor1),
              borderRadius: BorderRadius.all(Radius.circular(25.0)),
            ),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Palette.textColor1),
              borderRadius: BorderRadius.all(Radius.circular(25.0)),
            ),
            contentPadding: const EdgeInsets.all(10),
            hintStyle: const TextStyle(fontSize: 14, color: Palette.textColor1),
          )),
    );
  }
}

